package com.adventiel.challenge.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A Superheros.
 */
@Entity
@Table(name = "superheros")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Superheros implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "nom")
    private String nom;

    @Column(name = "vignette")
    private String vignette;

    @Column(name = "description")
    private String description;

    @ManyToOne
    @JsonIgnoreProperties(value = "superheros", allowSetters = true)
    private Superteam superteam;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public Superheros nom(String nom) {
        this.nom = nom;
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getVignette() {
        return vignette;
    }

    public Superheros vignette(String vignette) {
        this.vignette = vignette;
        return this;
    }

    public void setVignette(String vignette) {
        this.vignette = vignette;
    }

    public String getDescription() {
        return description;
    }

    public Superheros description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Superteam getSuperteam() {
        return superteam;
    }

    public Superheros superteam(Superteam superteam) {
        this.superteam = superteam;
        return this;
    }

    public void setSuperteam(Superteam superteam) {
        this.superteam = superteam;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here


    public Superheros() {
    }

    public Superheros(String nom, String description, String vignette) {
        this.nom = nom;
        this.vignette = vignette;
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Superheros)) {
            return false;
        }
        return id != null && id.equals(((Superheros) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Superheros{" +
            "id=" + getId() +
            ", nom='" + getNom() + "'" +
            ", vignette='" + getVignette() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
