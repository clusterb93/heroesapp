package com.adventiel.challenge.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Superteam.
 */
@Entity
@Table(name = "superteam")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Superteam implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "nom")
    private String nom;

    @OneToMany(mappedBy = "superteam")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Superheros> superheros = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public Superteam nom(String nom) {
        this.nom = nom;
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Set<Superheros> getSuperheros() {
        return superheros;
    }

    public Superteam superheros(Set<Superheros> superheros) {
        this.superheros = superheros;
        return this;
    }

    public Superteam addSuperheros(Superheros superheros) {
        this.superheros.add(superheros);
        superheros.setSuperteam(this);
        return this;
    }

    public Superteam removeSuperheros(Superheros superheros) {
        this.superheros.remove(superheros);
        superheros.setSuperteam(null);
        return this;
    }

    public void setSuperheros(Set<Superheros> superheros) {
        this.superheros = superheros;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Superteam)) {
            return false;
        }
        return id != null && id.equals(((Superteam) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Superteam{" +
            "id=" + getId() +
            ", nom='" + getNom() + "'" +
            "}";
    }
}
