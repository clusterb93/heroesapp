package com.adventiel.challenge.repository;

import com.adventiel.challenge.domain.Superheros;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the Superheros entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SuperherosRepository extends JpaRepository<Superheros, Long> {

    List<Superheros> findBySuperteamId(Long superteamId);
}
