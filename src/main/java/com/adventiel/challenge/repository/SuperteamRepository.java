package com.adventiel.challenge.repository;

import com.adventiel.challenge.domain.Superteam;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Superteam entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SuperteamRepository extends JpaRepository<Superteam, Long> {
}
