package com.adventiel.challenge.service;

import com.adventiel.challenge.domain.Superheros;
import com.adventiel.challenge.repository.SuperherosRepository;
import com.adventiel.challenge.service.dto.DataWrapper;
import com.adventiel.challenge.tools.MD5Util;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import com.adventiel.challenge.service.dto.Character;

import java.util.Date;
import java.util.List;

@Service
public class MarvelService {
    @Value("${marvel.base-url}")
    String baseUrl;

    @Value("${marvel.characters-url}")
    String charactersUrl;

    @Value("${marvel.public-key}")
    String publicKey;

    @Value("${marvel.private-key}")
    String privateKey;

    RestTemplate restTemplate;
    SuperherosRepository superherosRepository;

    public MarvelService(RestTemplate restTemplate, SuperherosRepository superherosRepository) {
        this.restTemplate = restTemplate;
        this.superherosRepository = superherosRepository;
    }

    public void initHeroesDatabase() {
        Date now = new Date();
        Long ts = now.getTime();
        String authParams = "?ts=%s&apikey=%s&hash=%s";
        ObjectMapper mapper = new ObjectMapper();

        String hash = MD5Util.hash(publicKey, privateKey, ts.toString());
        String url = baseUrl+charactersUrl+String.format(authParams, ts.toString(), publicKey, hash);
        ResponseEntity<DataWrapper<Character>> responseEntity = restTemplate.exchange(url, HttpMethod.GET, null,new ParameterizedTypeReference<DataWrapper<Character>>() {});
        if(responseEntity.getStatusCode() == HttpStatus.OK){
            List<Character> characters = mapper.convertValue(responseEntity.getBody().getData().getResults(), new TypeReference<List<Character>>() { });
            characters.stream().forEach(hero ->{superherosRepository.save(new Superheros(hero.getName(), hero.getDescription(), hero.getThumbnail().getPath()+"."+hero.getThumbnail().getExtension()));});
        }
    }
}
