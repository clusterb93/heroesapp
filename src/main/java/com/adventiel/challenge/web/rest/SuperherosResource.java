package com.adventiel.challenge.web.rest;

import com.adventiel.challenge.domain.Superheros;
import com.adventiel.challenge.repository.SuperherosRepository;
import com.adventiel.challenge.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.adventiel.challenge.domain.Superheros}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class SuperherosResource {

    private final Logger log = LoggerFactory.getLogger(SuperherosResource.class);

    private static final String ENTITY_NAME = "superheros";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SuperherosRepository superherosRepository;

    public SuperherosResource(SuperherosRepository superherosRepository) {
        this.superherosRepository = superherosRepository;
    }

    /**
     * {@code POST  /superheros} : Create a new superheros.
     *
     * @param superheros the superheros to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new superheros, or with status {@code 400 (Bad Request)} if the superheros has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/superheros")
    public ResponseEntity<Superheros> createSuperheros(@RequestBody Superheros superheros) throws URISyntaxException {
        log.debug("REST request to save Superheros : {}", superheros);
        if (superheros.getId() != null) {
            throw new BadRequestAlertException("A new superheros cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Superheros result = superherosRepository.save(superheros);
        return ResponseEntity.created(new URI("/api/superheros/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /superheros} : Updates an existing superheros.
     *
     * @param superheros the superheros to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated superheros,
     * or with status {@code 400 (Bad Request)} if the superheros is not valid,
     * or with status {@code 500 (Internal Server Error)} if the superheros couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/superheros")
    public ResponseEntity<Superheros> updateSuperheros(@RequestBody Superheros superheros) throws URISyntaxException {
        log.debug("REST request to update Superheros : {}", superheros);
        if (superheros.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Superheros result = superherosRepository.save(superheros);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, superheros.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /superheros} : get all the superheros.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of superheros in body.
     */
    @GetMapping("/superheros")
    public ResponseEntity<List<Superheros>> getAllSuperheros(Pageable pageable) {
        log.debug("REST request to get a page of Superheros");
        Page<Superheros> page = superherosRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /superheros/:id} : get the "id" superheros.
     *
     * @param id the id of the superheros to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the superheros, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/superheros/{id}")
    public ResponseEntity<Superheros> getSuperheros(@PathVariable Long id) {
        log.debug("REST request to get Superheros : {}", id);
        Optional<Superheros> superheros = superherosRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(superheros);
    }

    /**
     * {@code DELETE  /superheros/:id} : delete the "id" superheros.
     *
     * @param id the id of the superheros to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/superheros/{id}")
    public ResponseEntity<Void> deleteSuperheros(@PathVariable Long id) {
        log.debug("REST request to delete Superheros : {}", id);
        superherosRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
