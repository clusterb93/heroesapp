package com.adventiel.challenge.web.rest;

import com.adventiel.challenge.domain.Superheros;
import com.adventiel.challenge.domain.Superteam;
import com.adventiel.challenge.repository.SuperherosRepository;
import com.adventiel.challenge.repository.SuperteamRepository;
import com.adventiel.challenge.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.adventiel.challenge.domain.Superteam}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class SuperteamResource {

    private final Logger log = LoggerFactory.getLogger(SuperteamResource.class);

    private static final String ENTITY_NAME = "superteam";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SuperteamRepository superteamRepository;

    private final SuperherosRepository superherosRepository;

    public SuperteamResource(SuperteamRepository superteamRepository, SuperherosRepository superherosRepository) {
        this.superteamRepository = superteamRepository;
        this.superherosRepository =superherosRepository;
    }

    /**
     * {@code POST  /superteams} : Create a new superteam.
     *
     * @param superteam the superteam to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new superteam, or with status {@code 400 (Bad Request)} if the superteam has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/superteams")
    public ResponseEntity<Superteam> createSuperteam(@RequestBody Superteam superteam) throws URISyntaxException {
        log.debug("REST request to save Superteam : {}", superteam);
        if (superteam.getId() != null) {
            throw new BadRequestAlertException("A new superteam cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Superteam result = superteamRepository.save(superteam);
        return ResponseEntity.created(new URI("/api/superteams/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /superteams} : Updates an existing superteam.
     *
     * @param superteam the superteam to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated superteam,
     * or with status {@code 400 (Bad Request)} if the superteam is not valid,
     * or with status {@code 500 (Internal Server Error)} if the superteam couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/superteams")
    public ResponseEntity<Superteam> updateSuperteam(@RequestBody Superteam superteam) throws URISyntaxException {
        log.debug("REST request to update Superteam : {}", superteam);
        if (superteam.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Superteam result = superteamRepository.save(superteam);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, superteam.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /superteams} : get all the superteams.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of superteams in body.
     */
    @GetMapping("/superteams")
    public ResponseEntity<List<Superteam>> getAllSuperteams() {
        log.debug("REST request to get all Superteams");
        List<Superteam> superteams = superteamRepository.findAll();
        return ResponseEntity.ok().body(superteams);
    }

    /**
     * {@code GET  /superteams/:id} : get the "id" superteam.
     *
     * @param id the id of the superteam to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the superteam, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/superteams/{id}")
    public ResponseEntity<Superteam> getSuperteam(@PathVariable Long id) {
        log.debug("REST request to get Superteam : {}", id);
        Optional<Superteam> superteam = superteamRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(superteam);
    }

    /**
     * {@code GET  /superteams/:id} : get the "id" superteam.
     *
     * @param id the id of the superteam to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the superteam, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/superteams/{id}/superheros")
    public ResponseEntity<List<Superheros>> getSuperteamSuperheroes(@PathVariable Long id) {
        log.debug("REST request to get Superteam : {}", id);
        List<Superheros> superteamSuperheros = superherosRepository.findBySuperteamId(id);
        return ResponseEntity.ok().body(superteamSuperheros);
    }

    /**
     * {@code DELETE  /superteams/:id} : delete the "id" superteam.
     *
     * @param id the id of the superteam to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/superteams/{id}")
    public ResponseEntity<Void> deleteSuperteam(@PathVariable Long id) {
        log.debug("REST request to delete Superteam : {}", id);
        superteamRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    @DeleteMapping("/superteams/{idTeam}/superheros/{idSuperheros}")
    public ResponseEntity<Void> deleteSuperherosFromSuperteam(@PathVariable Long idTeam, @PathVariable Long idSuperheros) {
        log.debug("REST request to delete Suêrheros :{} from Superteam : {}", idSuperheros, idTeam);
        Optional<Superheros> superheros = superherosRepository.findById(idSuperheros);
        if(superheros.isPresent()){
            Superheros removedSuper = superheros.get();
            if(removedSuper.getId() == idSuperheros){
                removedSuper.setSuperteam(null);
                superherosRepository.save(removedSuper);
            }
        }
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, idSuperheros.toString())).build();
    }
}
