/**
 * View Models used by Spring MVC REST controllers.
 */
package com.adventiel.challenge.web.rest.vm;
