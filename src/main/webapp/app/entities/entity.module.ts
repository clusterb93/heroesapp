import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        loadChildren: () => import('./superheros/superheros.module').then(m => m.HeroesAppSuperherosModule),
      },
      {
        path: 'superheros',
        loadChildren: () => import('./superheros/superheros.module').then(m => m.HeroesAppSuperherosModule),
      },
      {
        path: 'superteam',
        loadChildren: () => import('./superteam/superteam.module').then(m => m.HeroesAppSuperteamModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class HeroesAppEntityModule {}
