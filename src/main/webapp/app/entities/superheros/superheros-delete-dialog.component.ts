import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ISuperheros } from 'app/shared/model/superheros.model';
import { SuperherosService } from './superheros.service';

@Component({
  templateUrl: './superheros-delete-dialog.component.html',
})
export class SuperherosDeleteDialogComponent {
  superheros?: ISuperheros;

  constructor(
    protected superherosService: SuperherosService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.superherosService.delete(id).subscribe(() => {
      this.eventManager.broadcast('superherosListModification');
      this.activeModal.close();
    });
  }
}
