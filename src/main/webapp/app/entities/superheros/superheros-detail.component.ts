import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ISuperheros } from 'app/shared/model/superheros.model';

@Component({
  selector: 'jhi-superheros-detail',
  templateUrl: './superheros-detail.component.html',
  styleUrls: ['superheros.scss'],
})
export class SuperherosDetailComponent implements OnInit {
  superheros: ISuperheros | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ superheros }) => (this.superheros = superheros));
  }

  previousState(): void {
    window.history.back();
  }
}
