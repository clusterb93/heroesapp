import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ISuperheros, Superheros } from 'app/shared/model/superheros.model';
import { SuperherosService } from './superheros.service';
import { ISuperteam } from 'app/shared/model/superteam.model';
import { SuperteamService } from 'app/entities/superteam/superteam.service';

@Component({
  selector: 'jhi-superheros-update',
  templateUrl: './superheros-update.component.html',
  styleUrls: ['superheros.scss'],
})
export class SuperherosUpdateComponent implements OnInit {
  isSaving = false;
  superteams: ISuperteam[] = [];

  editForm = this.fb.group({
    id: [],
    nom: [],
    vignette: [],
    description: [],
    superteam: [],
  });

  constructor(
    protected superherosService: SuperherosService,
    protected superteamService: SuperteamService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ superheros }) => {
      this.updateForm(superheros);

      this.superteamService.query().subscribe((res: HttpResponse<ISuperteam[]>) => (this.superteams = res.body || []));
    });
  }

  updateForm(superheros: ISuperheros): void {
    this.editForm.patchValue({
      id: superheros.id,
      nom: superheros.nom,
      vignette: superheros.vignette,
      description: superheros.description,
      superteam: superheros.superteam,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const superheros = this.createFromForm();
    if (superheros.id !== undefined) {
      this.subscribeToSaveResponse(this.superherosService.update(superheros));
    } else {
      this.subscribeToSaveResponse(this.superherosService.create(superheros));
    }
  }

  private createFromForm(): ISuperheros {
    return {
      ...new Superheros(),
      id: this.editForm.get(['id'])!.value,
      nom: this.editForm.get(['nom'])!.value,
      vignette: this.editForm.get(['vignette'])!.value,
      description: this.editForm.get(['description'])!.value,
      superteam: this.editForm.get(['superteam'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISuperheros>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: ISuperteam): any {
    return item.id;
  }
}
