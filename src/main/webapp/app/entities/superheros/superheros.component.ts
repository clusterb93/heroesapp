import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ISuperheros } from 'app/shared/model/superheros.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { SuperherosService } from './superheros.service';
import { SuperherosDeleteDialogComponent } from './superheros-delete-dialog.component';

@Component({
  selector: 'jhi-superheros',
  templateUrl: './superheros.component.html',
  styleUrls: ['superheros.scss'],
})
export class SuperherosComponent implements OnInit, OnDestroy {
  superheros: ISuperheros[];
  eventSubscriber?: Subscription;
  itemsPerPage: number;
  links: any;
  page: number;
  predicate: string;
  ascending: boolean;

  constructor(
    protected superherosService: SuperherosService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected parseLinks: JhiParseLinks
  ) {
    this.superheros = [];
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 0;
    this.links = {
      last: 0,
    };
    this.predicate = 'id';
    this.ascending = true;
  }

  loadAll(): void {
    this.superherosService
      .query({
        page: this.page,
        size: this.itemsPerPage,
        sort: this.sort(),
      })
      .subscribe((res: HttpResponse<ISuperheros[]>) => this.paginateSuperheros(res.body, res.headers));
  }

  reset(): void {
    this.page = 0;
    this.superheros = [];
    this.loadAll();
  }

  loadPage(page: number): void {
    this.page = page;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInSuperheros();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ISuperheros): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInSuperheros(): void {
    this.eventSubscriber = this.eventManager.subscribe('superherosListModification', () => this.reset());
  }

  delete(superheros: ISuperheros): void {
    const modalRef = this.modalService.open(SuperherosDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.superheros = superheros;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateSuperheros(data: ISuperheros[] | null, headers: HttpHeaders): void {
    const headersLink = headers.get('link');
    this.links = this.parseLinks.parse(headersLink ? headersLink : '');
    if (data) {
      for (let i = 0; i < data.length; i++) {
        this.superheros.push(data[i]);
      }
    }
  }
}
