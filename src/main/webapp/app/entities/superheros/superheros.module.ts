import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HeroesAppSharedModule } from 'app/shared/shared.module';
import { SuperherosComponent } from './superheros.component';
import { SuperherosDetailComponent } from './superheros-detail.component';
import { SuperherosUpdateComponent } from './superheros-update.component';
import { SuperherosDeleteDialogComponent } from './superheros-delete-dialog.component';
import { superherosRoute } from './superheros.route';

@NgModule({
  imports: [HeroesAppSharedModule, RouterModule.forChild(superherosRoute)],
  declarations: [SuperherosComponent, SuperherosDetailComponent, SuperherosUpdateComponent, SuperherosDeleteDialogComponent],
  entryComponents: [SuperherosDeleteDialogComponent],
})
export class HeroesAppSuperherosModule {}
