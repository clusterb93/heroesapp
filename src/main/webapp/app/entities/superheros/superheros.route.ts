import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { ISuperheros, Superheros } from 'app/shared/model/superheros.model';
import { SuperherosService } from './superheros.service';
import { SuperherosComponent } from './superheros.component';
import { SuperherosDetailComponent } from './superheros-detail.component';
import { SuperherosUpdateComponent } from './superheros-update.component';

@Injectable({ providedIn: 'root' })
export class SuperherosResolve implements Resolve<ISuperheros> {
  constructor(private service: SuperherosService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ISuperheros> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((superheros: HttpResponse<Superheros>) => {
          if (superheros.body) {
            return of(superheros.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Superheros());
  }
}

export const superherosRoute: Routes = [
  {
    path: '',
    component: SuperherosComponent,
    data: {
      pageTitle: 'heroesApp.superheros.home.title',
    }
  },
  {
    path: ':id/view',
    component: SuperherosDetailComponent,
    resolve: {
      superheros: SuperherosResolve,
    },
    data: {
      pageTitle: 'heroesApp.superheros.home.title',
    }
  },
  {
    path: 'new',
    component: SuperherosUpdateComponent,
    resolve: {
      superheros: SuperherosResolve,
    },
    data: {
      pageTitle: 'heroesApp.superheros.home.title',
    }
  },
  {
    path: ':id/edit',
    component: SuperherosUpdateComponent,
    resolve: {
      superheros: SuperherosResolve,
    },
    data: {
      pageTitle: 'heroesApp.superheros.home.title',
    }
  },
];
