import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ISuperheros } from 'app/shared/model/superheros.model';

type EntityResponseType = HttpResponse<ISuperheros>;
type EntityArrayResponseType = HttpResponse<ISuperheros[]>;

@Injectable({ providedIn: 'root' })
export class SuperherosService {
  public resourceUrl = SERVER_API_URL + 'api/superheros';

  constructor(protected http: HttpClient) {}

  create(superheros: ISuperheros): Observable<EntityResponseType> {
    return this.http.post<ISuperheros>(this.resourceUrl, superheros, { observe: 'response' });
  }

  update(superheros: ISuperheros): Observable<EntityResponseType> {
    return this.http.put<ISuperheros>(this.resourceUrl, superheros, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ISuperheros>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ISuperheros[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
