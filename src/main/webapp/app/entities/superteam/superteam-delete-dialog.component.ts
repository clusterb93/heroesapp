import {Component} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager} from 'ng-jhipster';

import {ISuperteam} from 'app/shared/model/superteam.model';
import {SuperteamService} from './superteam.service';

@Component({
  templateUrl: './superteam-delete-dialog.component.html',
})
export class SuperteamDeleteDialogComponent {
  superteam?: ISuperteam;

  constructor(protected superteamService: SuperteamService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {
  }

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(idTeam: number): void {
    this.superteamService.delete(idTeam).subscribe(() => {
      this.eventManager.broadcast('superteamListModification');
      this.activeModal.close();
    });
  }
}
