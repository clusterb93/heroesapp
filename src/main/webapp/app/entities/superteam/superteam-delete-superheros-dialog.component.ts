import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ISuperteam } from 'app/shared/model/superteam.model';
import { SuperteamService } from './superteam.service';
import {ISuperheros} from "../../shared/model/superheros.model";

@Component({
  templateUrl: './superteam-delete-superheros-dialog.component.html',
})
export class SuperteamDeleteSuperherosDialogComponent {
  superteam!: ISuperteam;
  superhero!: ISuperheros;

  constructor(protected superteamService: SuperteamService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(idTeam: number, idSuperheros: number): void {
      this.superteamService.deleteSuperherosFromTeam(idTeam,idSuperheros).subscribe(() => {
        this.eventManager.broadcast('superteamListModification');
        this.eventManager.broadcast('superherosListModification');
        this.activeModal.close();
      });
  }
}
