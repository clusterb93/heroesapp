import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ISuperteam } from 'app/shared/model/superteam.model';
import {ISuperheros} from "../../shared/model/superheros.model";
import {HttpResponse} from "@angular/common/http";
import {SuperteamService} from "./superteam.service";

@Component({
  selector: 'jhi-superteam-detail',
  templateUrl: './superteam-detail.component.html',
  styleUrls: ['superteam.scss'],
})
export class SuperteamDetailComponent implements OnInit {
  superteam: ISuperteam | null = null;
  superheros: ISuperheros[] | null = null;

  constructor(protected superteamService: SuperteamService, protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ superteam }) => {
      this.superteam = superteam;
      if(superteam.id){
        this.superteamService.findSuperheros(superteam.id).subscribe((res: HttpResponse<ISuperheros[]>) => (this.superteam!.superheros = res.body || []));
      }
    });

  }

  previousState(): void {
    window.history.back();
  }

  trackId(index: number, item: ISuperheros): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }
}
