import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ISuperteam, Superteam } from 'app/shared/model/superteam.model';
import { SuperteamService } from './superteam.service';
import {ISuperheros} from "../../shared/model/superheros.model";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {SuperteamDeleteSuperherosDialogComponent} from "./superteam-delete-superheros-dialog.component";

@Component({
  selector: 'jhi-superteam-update',
  templateUrl: './superteam-update.component.html',
  styleUrls: ['superteam.scss'],
})
export class SuperteamUpdateComponent implements OnInit {
  isSaving = false;
  superteam! : ISuperteam;

  editForm = this.fb.group({
    id: [],
    nom: [],
  });


  constructor(protected superteamService: SuperteamService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder, protected modalService: NgbModal) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ superteam }) => {
      this.updateForm(superteam);
      this.superteam = superteam;
      if(superteam.id){
        this.superteamService.findSuperheros(superteam.id).subscribe((res: HttpResponse<ISuperheros[]>) => (this.superteam.superheros = res.body || []));
      }
    });
  }

  updateForm(superteam: ISuperteam): void {
    this.editForm.patchValue({
      id: superteam.id,
      nom: superteam.nom,
    });
  }

  previousState(): void {
    window.history.back();
  }

  trackId(index: number, item: ISuperheros): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  save(): void {
    this.isSaving = true;
    const superteam = this.createFromForm();
    if (superteam.id !== undefined) {
      this.subscribeToSaveResponse(this.superteamService.update(superteam));
    } else {
      this.subscribeToSaveResponse(this.superteamService.create(superteam));
    }
  }

  private createFromForm(): ISuperteam {
    return {
      ...new Superteam(),
      id: this.editForm.get(['id'])!.value,
      nom: this.editForm.get(['nom'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISuperteam>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  deleteSuperheroFromTeam(superteam: ISuperteam, superhero: ISuperheros): void {
    const modalRef = this.modalService.open(SuperteamDeleteSuperherosDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.superteam = superteam;
    modalRef.componentInstance.superhero = superhero;
  }
}
