import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ISuperteam } from 'app/shared/model/superteam.model';
import { SuperteamService } from './superteam.service';
import { SuperteamDeleteDialogComponent } from './superteam-delete-dialog.component';

@Component({
  selector: 'jhi-superteam',
  templateUrl: './superteam.component.html',
})
export class SuperteamComponent implements OnInit, OnDestroy {
  superteams?: ISuperteam[];
  eventSubscriber?: Subscription;

  constructor(protected superteamService: SuperteamService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.superteamService.query().subscribe((res: HttpResponse<ISuperteam[]>) => (this.superteams = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInSuperteams();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ISuperteam): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInSuperteams(): void {
    this.eventSubscriber = this.eventManager.subscribe('superteamListModification', () => this.loadAll());
  }

  delete(superteam: ISuperteam): void {
    const modalRef = this.modalService.open(SuperteamDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.superteam = superteam;
  }

}
