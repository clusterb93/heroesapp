import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HeroesAppSharedModule } from 'app/shared/shared.module';
import { SuperteamComponent } from './superteam.component';
import { SuperteamDetailComponent } from './superteam-detail.component';
import { SuperteamUpdateComponent } from './superteam-update.component';
import { SuperteamDeleteDialogComponent } from './superteam-delete-dialog.component';
import { superteamRoute } from './superteam.route';
import {SuperteamDeleteSuperherosDialogComponent} from "./superteam-delete-superheros-dialog.component";

@NgModule({
  imports: [HeroesAppSharedModule, RouterModule.forChild(superteamRoute)],
  declarations: [SuperteamComponent, SuperteamDetailComponent, SuperteamUpdateComponent, SuperteamDeleteDialogComponent, SuperteamDeleteSuperherosDialogComponent],
  entryComponents: [SuperteamDeleteDialogComponent, SuperteamDeleteSuperherosDialogComponent],
})
export class HeroesAppSuperteamModule {}
