import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { ISuperteam, Superteam } from 'app/shared/model/superteam.model';
import { SuperteamService } from './superteam.service';
import { SuperteamComponent } from './superteam.component';
import { SuperteamDetailComponent } from './superteam-detail.component';
import { SuperteamUpdateComponent } from './superteam-update.component';

@Injectable({ providedIn: 'root' })
export class SuperteamResolve implements Resolve<ISuperteam> {
  constructor(private service: SuperteamService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ISuperteam> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((superteam: HttpResponse<Superteam>) => {
          if (superteam.body) {
            return of(superteam.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Superteam());
  }
}

export const superteamRoute: Routes = [
  {
    path: '',
    component: SuperteamComponent,
    data: {
      pageTitle: 'heroesApp.superteam.home.title',
    }
  },
  {
    path: ':id/view',
    component: SuperteamDetailComponent,
    resolve: {
      superteam: SuperteamResolve,
    },
    data: {
      pageTitle: 'heroesApp.superteam.home.title',
    }
  },
  {
    path: 'new',
    component: SuperteamUpdateComponent,
    resolve: {
      superteam: SuperteamResolve,
    },
    data: {
      pageTitle: 'heroesApp.superteam.home.title',
    }
  },
  {
    path: ':id/edit',
    component: SuperteamUpdateComponent,
    resolve: {
      superteam: SuperteamResolve,
    },
    data: {
      pageTitle: 'heroesApp.superteam.home.title',
    }
  },
];
