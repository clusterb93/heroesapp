import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ISuperteam } from 'app/shared/model/superteam.model';
import {ISuperheros} from "../../shared/model/superheros.model";

type EntityResponseType = HttpResponse<ISuperteam>;
type EntityArrayResponseType = HttpResponse<ISuperteam[]>;

@Injectable({ providedIn: 'root' })
export class SuperteamService {
  public resourceUrl = SERVER_API_URL + 'api/superteams';

  constructor(protected http: HttpClient) {}

  create(superteam: ISuperteam): Observable<EntityResponseType> {
    return this.http.post<ISuperteam>(this.resourceUrl, superteam, { observe: 'response' });
  }

  update(superteam: ISuperteam): Observable<EntityResponseType> {
    return this.http.put<ISuperteam>(this.resourceUrl, superteam, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ISuperteam>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ISuperteam[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  findSuperheros(id: number): Observable<EntityArrayResponseType> {
    return this.http.get<ISuperheros[]>(`${this.resourceUrl}/${id}/superheros`, { observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  deleteSuperherosFromTeam(idTeam: number, idSuperheros: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${idTeam}/superheros/${idSuperheros}`, { observe: 'response' });
  }
}
