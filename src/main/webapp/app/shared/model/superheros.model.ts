import { ISuperteam } from 'app/shared/model/superteam.model';

export interface ISuperheros {
  id?: number;
  nom?: string;
  vignette?: string;
  description?: string;
  superteam?: ISuperteam;
}

export class Superheros implements ISuperheros {
  constructor(
    public id?: number,
    public nom?: string,
    public vignette?: string,
    public description?: string,
    public superteam?: ISuperteam
  ) {}
}
