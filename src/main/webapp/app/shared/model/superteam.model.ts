import { ISuperheros } from 'app/shared/model/superheros.model';

export interface ISuperteam {
  id?: number;
  nom?: string;
  superheros?: ISuperheros[];
}

export class Superteam implements ISuperteam {
  constructor(public id?: number, public nom?: string, public superheros?: ISuperheros[]) {}
}
