package com.adventiel.challenge.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.adventiel.challenge.web.rest.TestUtil;

public class SuperherosTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Superheros.class);
        Superheros superheros1 = new Superheros();
        superheros1.setId(1L);
        Superheros superheros2 = new Superheros();
        superheros2.setId(superheros1.getId());
        assertThat(superheros1).isEqualTo(superheros2);
        superheros2.setId(2L);
        assertThat(superheros1).isNotEqualTo(superheros2);
        superheros1.setId(null);
        assertThat(superheros1).isNotEqualTo(superheros2);
    }
}
