package com.adventiel.challenge.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.adventiel.challenge.web.rest.TestUtil;

public class SuperteamTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Superteam.class);
        Superteam superteam1 = new Superteam();
        superteam1.setId(1L);
        Superteam superteam2 = new Superteam();
        superteam2.setId(superteam1.getId());
        assertThat(superteam1).isEqualTo(superteam2);
        superteam2.setId(2L);
        assertThat(superteam1).isNotEqualTo(superteam2);
        superteam1.setId(null);
        assertThat(superteam1).isNotEqualTo(superteam2);
    }
}
