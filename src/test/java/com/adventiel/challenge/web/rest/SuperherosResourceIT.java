package com.adventiel.challenge.web.rest;

import com.adventiel.challenge.HeroesApp;
import com.adventiel.challenge.domain.Superheros;
import com.adventiel.challenge.repository.SuperherosRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link SuperherosResource} REST controller.
 */
@SpringBootTest(classes = HeroesApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class SuperherosResourceIT {

    private static final String DEFAULT_NOM = "AAAAAAAAAA";
    private static final String UPDATED_NOM = "BBBBBBBBBB";

    private static final String DEFAULT_VIGNETTE = "AAAAAAAAAA";
    private static final String UPDATED_VIGNETTE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private SuperherosRepository superherosRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restSuperherosMockMvc;

    private Superheros superheros;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Superheros createEntity(EntityManager em) {
        Superheros superheros = new Superheros()
            .nom(DEFAULT_NOM)
            .vignette(DEFAULT_VIGNETTE)
            .description(DEFAULT_DESCRIPTION);
        return superheros;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Superheros createUpdatedEntity(EntityManager em) {
        Superheros superheros = new Superheros()
            .nom(UPDATED_NOM)
            .vignette(UPDATED_VIGNETTE)
            .description(UPDATED_DESCRIPTION);
        return superheros;
    }

    @BeforeEach
    public void initTest() {
        superheros = createEntity(em);
    }

    @Test
    @Transactional
    public void createSuperheros() throws Exception {
        int databaseSizeBeforeCreate = superherosRepository.findAll().size();
        // Create the Superheros
        restSuperherosMockMvc.perform(post("/api/superheros")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(superheros)))
            .andExpect(status().isCreated());

        // Validate the Superheros in the database
        List<Superheros> superherosList = superherosRepository.findAll();
        assertThat(superherosList).hasSize(databaseSizeBeforeCreate + 1);
        Superheros testSuperheros = superherosList.get(superherosList.size() - 1);
        assertThat(testSuperheros.getNom()).isEqualTo(DEFAULT_NOM);
        assertThat(testSuperheros.getVignette()).isEqualTo(DEFAULT_VIGNETTE);
        assertThat(testSuperheros.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createSuperherosWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = superherosRepository.findAll().size();

        // Create the Superheros with an existing ID
        superheros.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSuperherosMockMvc.perform(post("/api/superheros")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(superheros)))
            .andExpect(status().isBadRequest());

        // Validate the Superheros in the database
        List<Superheros> superherosList = superherosRepository.findAll();
        assertThat(superherosList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllSuperheros() throws Exception {
        // Initialize the database
        superherosRepository.saveAndFlush(superheros);

        // Get all the superherosList
        restSuperherosMockMvc.perform(get("/api/superheros?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(superheros.getId().intValue())))
            .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM)))
            .andExpect(jsonPath("$.[*].vignette").value(hasItem(DEFAULT_VIGNETTE)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)));
    }
    
    @Test
    @Transactional
    public void getSuperheros() throws Exception {
        // Initialize the database
        superherosRepository.saveAndFlush(superheros);

        // Get the superheros
        restSuperherosMockMvc.perform(get("/api/superheros/{id}", superheros.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(superheros.getId().intValue()))
            .andExpect(jsonPath("$.nom").value(DEFAULT_NOM))
            .andExpect(jsonPath("$.vignette").value(DEFAULT_VIGNETTE))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION));
    }
    @Test
    @Transactional
    public void getNonExistingSuperheros() throws Exception {
        // Get the superheros
        restSuperherosMockMvc.perform(get("/api/superheros/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSuperheros() throws Exception {
        // Initialize the database
        superherosRepository.saveAndFlush(superheros);

        int databaseSizeBeforeUpdate = superherosRepository.findAll().size();

        // Update the superheros
        Superheros updatedSuperheros = superherosRepository.findById(superheros.getId()).get();
        // Disconnect from session so that the updates on updatedSuperheros are not directly saved in db
        em.detach(updatedSuperheros);
        updatedSuperheros
            .nom(UPDATED_NOM)
            .vignette(UPDATED_VIGNETTE)
            .description(UPDATED_DESCRIPTION);

        restSuperherosMockMvc.perform(put("/api/superheros")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedSuperheros)))
            .andExpect(status().isOk());

        // Validate the Superheros in the database
        List<Superheros> superherosList = superherosRepository.findAll();
        assertThat(superherosList).hasSize(databaseSizeBeforeUpdate);
        Superheros testSuperheros = superherosList.get(superherosList.size() - 1);
        assertThat(testSuperheros.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testSuperheros.getVignette()).isEqualTo(UPDATED_VIGNETTE);
        assertThat(testSuperheros.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingSuperheros() throws Exception {
        int databaseSizeBeforeUpdate = superherosRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSuperherosMockMvc.perform(put("/api/superheros")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(superheros)))
            .andExpect(status().isBadRequest());

        // Validate the Superheros in the database
        List<Superheros> superherosList = superherosRepository.findAll();
        assertThat(superherosList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSuperheros() throws Exception {
        // Initialize the database
        superherosRepository.saveAndFlush(superheros);

        int databaseSizeBeforeDelete = superherosRepository.findAll().size();

        // Delete the superheros
        restSuperherosMockMvc.perform(delete("/api/superheros/{id}", superheros.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Superheros> superherosList = superherosRepository.findAll();
        assertThat(superherosList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
