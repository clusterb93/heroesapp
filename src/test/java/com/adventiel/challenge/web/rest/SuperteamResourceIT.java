package com.adventiel.challenge.web.rest;

import com.adventiel.challenge.HeroesApp;
import com.adventiel.challenge.domain.Superteam;
import com.adventiel.challenge.repository.SuperteamRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link SuperteamResource} REST controller.
 */
@SpringBootTest(classes = HeroesApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class SuperteamResourceIT {

    private static final String DEFAULT_NOM = "AAAAAAAAAA";
    private static final String UPDATED_NOM = "BBBBBBBBBB";

    @Autowired
    private SuperteamRepository superteamRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restSuperteamMockMvc;

    private Superteam superteam;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Superteam createEntity(EntityManager em) {
        Superteam superteam = new Superteam()
            .nom(DEFAULT_NOM);
        return superteam;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Superteam createUpdatedEntity(EntityManager em) {
        Superteam superteam = new Superteam()
            .nom(UPDATED_NOM);
        return superteam;
    }

    @BeforeEach
    public void initTest() {
        superteam = createEntity(em);
    }

    @Test
    @Transactional
    public void createSuperteam() throws Exception {
        int databaseSizeBeforeCreate = superteamRepository.findAll().size();
        // Create the Superteam
        restSuperteamMockMvc.perform(post("/api/superteams")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(superteam)))
            .andExpect(status().isCreated());

        // Validate the Superteam in the database
        List<Superteam> superteamList = superteamRepository.findAll();
        assertThat(superteamList).hasSize(databaseSizeBeforeCreate + 1);
        Superteam testSuperteam = superteamList.get(superteamList.size() - 1);
        assertThat(testSuperteam.getNom()).isEqualTo(DEFAULT_NOM);
    }

    @Test
    @Transactional
    public void createSuperteamWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = superteamRepository.findAll().size();

        // Create the Superteam with an existing ID
        superteam.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSuperteamMockMvc.perform(post("/api/superteams")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(superteam)))
            .andExpect(status().isBadRequest());

        // Validate the Superteam in the database
        List<Superteam> superteamList = superteamRepository.findAll();
        assertThat(superteamList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllSuperteams() throws Exception {
        // Initialize the database
        superteamRepository.saveAndFlush(superteam);

        // Get all the superteamList
        restSuperteamMockMvc.perform(get("/api/superteams?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(superteam.getId().intValue())))
            .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM)));
    }
    
    @Test
    @Transactional
    public void getSuperteam() throws Exception {
        // Initialize the database
        superteamRepository.saveAndFlush(superteam);

        // Get the superteam
        restSuperteamMockMvc.perform(get("/api/superteams/{id}", superteam.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(superteam.getId().intValue()))
            .andExpect(jsonPath("$.nom").value(DEFAULT_NOM));
    }
    @Test
    @Transactional
    public void getNonExistingSuperteam() throws Exception {
        // Get the superteam
        restSuperteamMockMvc.perform(get("/api/superteams/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSuperteam() throws Exception {
        // Initialize the database
        superteamRepository.saveAndFlush(superteam);

        int databaseSizeBeforeUpdate = superteamRepository.findAll().size();

        // Update the superteam
        Superteam updatedSuperteam = superteamRepository.findById(superteam.getId()).get();
        // Disconnect from session so that the updates on updatedSuperteam are not directly saved in db
        em.detach(updatedSuperteam);
        updatedSuperteam
            .nom(UPDATED_NOM);

        restSuperteamMockMvc.perform(put("/api/superteams")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedSuperteam)))
            .andExpect(status().isOk());

        // Validate the Superteam in the database
        List<Superteam> superteamList = superteamRepository.findAll();
        assertThat(superteamList).hasSize(databaseSizeBeforeUpdate);
        Superteam testSuperteam = superteamList.get(superteamList.size() - 1);
        assertThat(testSuperteam.getNom()).isEqualTo(UPDATED_NOM);
    }

    @Test
    @Transactional
    public void updateNonExistingSuperteam() throws Exception {
        int databaseSizeBeforeUpdate = superteamRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSuperteamMockMvc.perform(put("/api/superteams")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(superteam)))
            .andExpect(status().isBadRequest());

        // Validate the Superteam in the database
        List<Superteam> superteamList = superteamRepository.findAll();
        assertThat(superteamList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSuperteam() throws Exception {
        // Initialize the database
        superteamRepository.saveAndFlush(superteam);

        int databaseSizeBeforeDelete = superteamRepository.findAll().size();

        // Delete the superteam
        restSuperteamMockMvc.perform(delete("/api/superteams/{id}", superteam.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Superteam> superteamList = superteamRepository.findAll();
        assertThat(superteamList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
