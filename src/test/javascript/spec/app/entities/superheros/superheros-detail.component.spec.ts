import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { HeroesAppTestModule } from '../../../test.module';
import { SuperherosDetailComponent } from 'app/entities/superheros/superheros-detail.component';
import { Superheros } from 'app/shared/model/superheros.model';

describe('Component Tests', () => {
  describe('Superheros Management Detail Component', () => {
    let comp: SuperherosDetailComponent;
    let fixture: ComponentFixture<SuperherosDetailComponent>;
    const route = ({ data: of({ superheros: new Superheros(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HeroesAppTestModule],
        declarations: [SuperherosDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(SuperherosDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(SuperherosDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load superheros on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.superheros).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
