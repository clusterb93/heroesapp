import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { HeroesAppTestModule } from '../../../test.module';
import { SuperherosUpdateComponent } from 'app/entities/superheros/superheros-update.component';
import { SuperherosService } from 'app/entities/superheros/superheros.service';
import { Superheros } from 'app/shared/model/superheros.model';

describe('Component Tests', () => {
  describe('Superheros Management Update Component', () => {
    let comp: SuperherosUpdateComponent;
    let fixture: ComponentFixture<SuperherosUpdateComponent>;
    let service: SuperherosService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HeroesAppTestModule],
        declarations: [SuperherosUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(SuperherosUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(SuperherosUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(SuperherosService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Superheros(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Superheros();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
