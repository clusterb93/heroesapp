import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { HeroesAppTestModule } from '../../../test.module';
import { SuperteamDetailComponent } from 'app/entities/superteam/superteam-detail.component';
import { Superteam } from 'app/shared/model/superteam.model';

describe('Component Tests', () => {
  describe('Superteam Management Detail Component', () => {
    let comp: SuperteamDetailComponent;
    let fixture: ComponentFixture<SuperteamDetailComponent>;
    const route = ({ data: of({ superteam: new Superteam(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HeroesAppTestModule],
        declarations: [SuperteamDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(SuperteamDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(SuperteamDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load superteam on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.superteam).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
