import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { HeroesAppTestModule } from '../../../test.module';
import { SuperteamUpdateComponent } from 'app/entities/superteam/superteam-update.component';
import { SuperteamService } from 'app/entities/superteam/superteam.service';
import { Superteam } from 'app/shared/model/superteam.model';

describe('Component Tests', () => {
  describe('Superteam Management Update Component', () => {
    let comp: SuperteamUpdateComponent;
    let fixture: ComponentFixture<SuperteamUpdateComponent>;
    let service: SuperteamService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HeroesAppTestModule],
        declarations: [SuperteamUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(SuperteamUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(SuperteamUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(SuperteamService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Superteam(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Superteam();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
