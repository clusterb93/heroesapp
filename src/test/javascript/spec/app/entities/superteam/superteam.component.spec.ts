import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { HeroesAppTestModule } from '../../../test.module';
import { SuperteamComponent } from 'app/entities/superteam/superteam.component';
import { SuperteamService } from 'app/entities/superteam/superteam.service';
import { Superteam } from 'app/shared/model/superteam.model';

describe('Component Tests', () => {
  describe('Superteam Management Component', () => {
    let comp: SuperteamComponent;
    let fixture: ComponentFixture<SuperteamComponent>;
    let service: SuperteamService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HeroesAppTestModule],
        declarations: [SuperteamComponent],
      })
        .overrideTemplate(SuperteamComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(SuperteamComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(SuperteamService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Superteam(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.superteams && comp.superteams[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
